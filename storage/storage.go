package storage

import (
	"database/sql"
	"errors"
	"fmt"
	"time"
)


type UserData struct {
	Tru int64
	Fal int64
}

// User steps
const (
	Menuoynasi    string = "menu"
	Balanstekshirish string = "balans"
	OvozBerish      string = "ovozberish"
	RegisteredStep       string = "registered"
)
//CREATE TABLE IF NOT EXISTS "phonenumbers" (
//   "tg_id" INT NOT NULL,
//    "fullname" VARCHAR(100),
//    "phone_number" VARCHAR(50) UNIQUE,
//    "status" BOOLEAN,
//    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
//)

type User struct {
	TgID        int64
	TgName      string
	Fullname    *string
	PhoneNumber *string
	Step        string
	CreatedAt   *time.Time
}

type StorageI interface {
	Create(u *User) (*User, error)
	Get(id int64) (*User, error)
	GetOrCreate(tgID int64, tgName string) (*User, error)
	ChangeField(tgID int64, field, value string) error
	ChangeStep(tgID int64, step string) error
	SelectCur(tgID int64, course string) error
	INsertPhoneNumber(user *User, phonenumber string) error 
	GetphoneNumbers(user *User) (*UserData, error)
	GetPhoneNumbersUserMoney(user *User) (*int64, error)
}

type storagePg struct {
	db *sql.DB
}

func NewStoragePg(db *sql.DB) StorageI {
	return &storagePg{
		db: db,
	}
}

func (s *storagePg) SelectCur(tgID int64, course string) error {
	fmt.Println(tgID, course)
	query := `INSERT INTO curses(
		tgid,
		curse
	) VALUES ($1, $2) RETURNING tgid, curse`
	_ = s.db.QueryRow(query, tgID, course).Scan()
	return nil
}



func (s *storagePg) GetPhoneNumbersUserMoney(user *User) (*int64, error) {
	var qabulqilngan int64
	query:= `
	SELECT 
		COUNT(*) 
 	FROM 
	 	phonenumbers
 	WHERE
	 	tg_id=$1 AND status=$2 AND tulov IS NULL;	
	`
	err := s.db.QueryRow(query, user.TgID, true).Scan(&qabulqilngan)
	if err != nil {
		fmt.Println(err)
	}
	return &qabulqilngan, nil
}


func (s *storagePg) GetphoneNumbers(user *User) (*UserData, error) {
	var result UserData
	var qabulqilinmagan int64
	var qabulqilngan int64
	query:= `
	SELECT 
		COUNT(*) 
 	FROM 
	 	phonenumbers
 	WHERE
	 	tg_id=$1 AND status=$2 AND tulov IS NULL;	
	`
	err := s.db.QueryRow(query, user.TgID, true).Scan(&qabulqilngan)
	if err != nil {
		fmt.Println(err)
	}

	err = s.db.QueryRow(query, user.TgID, false).Scan(&qabulqilinmagan)
	if err != nil {
		fmt.Println(err)
	}
	result.Tru=qabulqilngan
	result.Fal=qabulqilinmagan
	return &result, nil
}

//CREATE TABLE IF NOT EXISTS "users"(
//    "tg_id" INT NOT NULL,
//    "tg_name" VARCHAR(50) NOT NULL,
//    "fullname" VARCHAR(100),
//    "step" VARCHAR(50) NOT NULL,
//    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
//);
func (s *storagePg) Create(user *User) (*User, error) {
	query := `
		INSERT INTO users(
			tg_id,
			tg_name,
			step
		) VALUES($1, $2, $3)
		RETURNING
			fullname,
			phone_number,
			created_at
	`

	err := s.db.QueryRow(
		query,
		user.TgID,
		user.TgName,
		user.Step,
	).Scan(
		&user.Fullname,
		&user.PhoneNumber,
		&user.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (s *storagePg) Get(id int64) (*User, error) {
	var result User

	query := `
		SELECT
			tg_id,
			fullname,
			phone_number,
			step,
			created_at
		FROM users
		WHERE tg_id=$1
	`

	row := s.db.QueryRow(query, id)
	err := row.Scan(
		&result.TgID,
		&result.Fullname,
		&result.PhoneNumber,
		&result.Step,
		&result.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (s *storagePg) GetOrCreate(tgID int64, tgName string) (*User, error) {
	user, err := s.Get(tgID)
	if errors.Is(err, sql.ErrNoRows) {
		u, err := s.Create(&User{
			TgID:   tgID,
			TgName: tgName,
			Step:   Menuoynasi,
		})
		if err != nil {
			return nil, err
		}

		user = u
	} else if err != nil {
		return nil, err
	}

	return user, nil
}

//CREATE TABLE IF NOT EXISTS "phonenumbers" (
//   "tg_id" INT NOT NULL,
//    "fullname" VARCHAR(100),
//    "phone_number" VARCHAR(50) UNIQUE,
//    "status" BOOLEAN,
//    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
//)

func (s *storagePg) INsertPhoneNumber(user *User, phonenumber string) error {
	query := `INSERT INTO phonenumbers(tg_id, fullname, phone_number, status) VALUES ($1, $2, $3, $4)`
	_, err := s.db.Exec(query, user.TgID, user.Fullname, phonenumber, false)
	if err != nil {
		fmt.Println(err)
	}
	return nil
}

func (s *storagePg) ChangeField(tgID int64, field, value string) error {
	query := fmt.Sprintf("UPDATE users SET %s=$1 WHERE tg_id=$2", field)

	result, err := s.db.Exec(query, value, tgID)
	if err != nil {
		return err
	}

	if count, _ := result.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (s *storagePg) ChangeStep(tgID int64, step string) error {
	query := "UPDATE users SET step=$1 WHERE tg_id=$2"

	result, err := s.db.Exec(query, step, tgID)
	if err != nil {
		return err
	}

	if count, _ := result.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}
