package bot

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

var MenuButtons = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("📨 Ovoz berish"),
		tgbotapi.NewKeyboardButton("📺 Botdan foydalanish"),
		tgbotapi.NewKeyboardButton("💸 To'lovlar"),
	),
		
)

var Objects = tgbotapi.NewInlineKeyboardMarkup(
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonURL("🔗 SMS orqli", "https://openbudget.uz/boards/initiatives/initiative/31/8fee4a4d-2ec1-4c7c-81ed-ff905e8d6034"),
		tgbotapi.NewInlineKeyboardButtonURL("🗿 Telegram Bot", "https://t.me/ochiqbudjet_2_bot?start=031274409005"),
	),
)

var BalansButtons = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("💳 Yechib olish"),
		tgbotapi.NewKeyboardButton("🔙 Orqaga"),
	),
)

