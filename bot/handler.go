package bot

import (
	"fmt"
	"log"
	"strings"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/openbot/storage"
)

func (h *BotHandler) Menu(user *storage.User, text string) error {
	if text == "📨 Ovoz berish" {
		return h.Ovozberish(user)
	} 
	if text == "📺 Botdan foydalanish" {
		return h.BotVideo(user)
	} 
	if text == "💸 To'lovlar" {
		return h.Tolovlar(user)
	} 
	err := h.storage.ChangeStep(user.TgID, storage.Menuoynasi)
	if err != nil {
		return err
	}

	msg := tgbotapi.NewMessage(user.TgID, Menumessage)
	msg.ReplyMarkup = MenuButtons
	if _, err := h.bot.Send(msg); err != nil {
		log.Println(err)
	}

	return nil
}

func(h *BotHandler) Register(user *storage.User, text string) error {
	check:=strings.Split(text, "")
	if len(check)<13 || len(check)>13 || check[0] != "+" {
		msg := tgbotapi.NewMessage(user.TgID, "Raqam to'g'riligini tekshiring(+998900000000)")
		msg.ReplyMarkup = MenuButtons
		if _, err := h.bot.Send(msg); err != nil {
			log.Println(err)
		}
	}
	err := h.storage.ChangeField(user.TgID, "phone_number", text)
	if err != nil {
		fmt.Println(err)
	}

	return h.Menu(user, "")
}

func(h *BotHandler) BotVideo(user *storage.User) error {
	msg := tgbotapi.NewVideo(user.TgID, tgbotapi.FileURL("https://open-bucket22.s3.eu-north-1.amazonaws.com/video_2023-09-05_18-16-37.mp4"))
	if _, err := h.bot.Send(msg); err != nil {
		log.Println(err)
	}
	return nil
}

func(h *BotHandler) Registered(user *storage.User) error {
	err := h.storage.ChangeStep(user.TgID, storage.RegisteredStep)
	if err != nil {
		return err
	}

	return nil
}


func (h *BotHandler) Tolovlar(user *storage.User) error {
	result1, err := h.storage.GetphoneNumbers(user)
	if err != nil {
		fmt.Println(err)
	}
	msg := tgbotapi.NewMessage(user.TgID, fmt.Sprintf("Tasdiqlangan ovozlar soni %v ✅\nTekshiruvdagi ovozlar soni %v ⏳\nQuyida faqat sizga tulov amalga oshirilmagan ovozlar chiqariladi!\nAgar sizning ovozingiz tasdiqlangan va tulov amalga oshirilgan bo'lsa ular ko'rinmaydi", result1.Tru, result1.Fal),)
	msg.ReplyMarkup = MenuButtons
	if _, err := h.bot.Send(msg); err != nil {
		log.Println(err)
	}
	return nil
}

func (h *BotHandler) ReferalK(user *storage.User) error {
	msg := tgbotapi.NewMessage(user.TgID, Refiralsilka)
	msg.ReplyMarkup = MenuButtons
	if _, err := h.bot.Send(msg); err != nil {
		log.Println(err)
	}
	return nil
}


func (h *BotHandler) OvozQabulqilibolish(user *storage.User, text string) error {
	//+998933646909
	check:=strings.Split(text, "")
	if len(check)<13 || len(check)>13 || check[0] != "+" {
		msg := tgbotapi.NewMessage(user.TgID, "Raqam to'g'riligini tekshiring(+998900000000)")
		msg.ReplyMarkup = MenuButtons
		if _, err := h.bot.Send(msg); err != nil {
			log.Println(err)
		}
	}
	h.storage.INsertPhoneNumber(user, text)
	err := h.storage.ChangeStep(user.TgID, storage.Menuoynasi)
	if err != nil {
		return err
	}

	
	msg := tgbotapi.NewMessage(user.TgID,"Ovoz berganingiz uchun raxmat!!!\n\nRaqamingiz tekshirishga yuborildi 24 soat ichida tasdiqlanadi")
	msg.ReplyMarkup = MenuButtons
	if _, err := h.bot.Send(msg); err != nil {
		log.Println(err)
	}

	return h.Menu(user, text)
}

func (h *BotHandler) Ovozberish(user *storage.User) error {
	err := h.storage.ChangeStep(user.TgID, storage.OvozBerish)
	if err != nil {
		return err
	}

	msg := tgbotapi.NewPhoto(user.TgID, tgbotapi.FileURL("https://openbudget.uz/api/v2/info/file/fb76dc21612c9943fdc123aed8049e07"))
	
	msg.Caption=Object1Text
	msg.ReplyMarkup = Objects
	if _, err := h.bot.Send(msg); err != nil {
		log.Println(err)
	}


	ms := tgbotapi.NewMessage(user.TgID,"Ovoz berganingizdan so'ng ovoz bergan raqamingizni yozib qoldiring +998900000000 shaklida")
	ms.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
	if _, err := h.bot.Send(ms); err != nil {
		log.Println(err)
	}
return nil
}

func (h *BotHandler) BalansTekshir(user *storage.User, text string) error {
	if text == "💳 Yechib olish" {
		fmt.Println("balans yechib olish")
	} 
	if text == "🔙 Orqagaa" {
		fmt.Println("orqaga")
	} 

	err := h.storage.ChangeStep(user.TgID, storage.Menuoynasi)
	if err != nil {
		return err
	}
	return h.Menu(user, text)
}

func (h *BotHandler) Balans(user *storage.User) error {
	err := h.storage.ChangeStep(user.TgID, storage.Balanstekshirish)
	if err != nil {
		return err
	}

	msg := tgbotapi.NewMessage(user.TgID,"amalni tanlang")
	msg.ReplyMarkup = BalansButtons
	if _, err := h.bot.Send(msg); err != nil {
		log.Println(err)
	}

	return nil
}