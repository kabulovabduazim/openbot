package bot

import (
	"fmt"
	"log"

	"gitlab.com/openbot/config"
	"gitlab.com/openbot/storage"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type BotHandler struct {
	cfg     config.Config
	storage storage.StorageI
	bot     *tgbotapi.BotAPI
}

func New(cfg config.Config, strg storage.StorageI) BotHandler {
	bot, err := tgbotapi.NewBotAPI("5845161728:AAFqEYQ0G2IJ_gYt337DD2Sw35YpIchsrCs")
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true

	return BotHandler{
		cfg:     cfg,
		storage: strg,
		bot:     bot,
	}
}

func (h *BotHandler) Start() {
	log.Printf("Authorized on account %s", h.bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := h.bot.GetUpdatesChan(u)

	for update := range updates {
		go h.HandleBot(update)
	}
}

func (h *BotHandler) HandleBot(update tgbotapi.Update) {
	user, err := h.storage.GetOrCreate(update.Message.From.ID, update.Message.From.FirstName)
	if err != nil {
		log.Println("failed to call storage.GetOrCreate: ", err)
		h.SendMessage(user, "error happened")
	}
	if update.Message.Command() == "start" {
		msg := tgbotapi.NewMessage(user.TgID, "Botdan ro'yxatdan o'tishingiz kerak buladi buning uchun telefon raqamingizni kiriting.\nSiz amalga oshirgan barcha ovozlar to'lovi quyida siz ro'yxatdan o'tgan raqamga to'lab beriladi!!!")
		if _, err := h.bot.Send(msg); err != nil {
			log.Println(err)
		}
		err = h.Registered(user)
	} else if update.Message.Text != "" {
		switch user.Step {
		case storage.RegisteredStep:
			err= h.Register(user, update.Message.Text)
		case storage.OvozBerish:
			err =h.OvozQabulqilibolish(user, update.Message.Text)
		case storage.Menuoynasi:
			err = h.Menu(user, update.Message.Text)
		case storage.Balanstekshirish:
			err = h.BalansTekshir(user, update.Message.Text)
		default:
			h.SendMessage(user, "Iltimos bot qaytadan ishga tushiring /start")
		}
	}

	if err != nil {
		log.Println("failed to handle message: ", err)
		h.SendMessage(user, "error happened")
	}
}

func (h *BotHandler) SendMessage(user *storage.User, message string) {
	fmt.Println(user)
	msg := tgbotapi.NewMessage(user.TgID, message)
	if _, err := h.bot.Send(msg); err != nil {
		log.Println(err)
	}
}
