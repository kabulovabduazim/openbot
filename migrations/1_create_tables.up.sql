CREATE TABLE IF NOT EXISTS "users"(
    "tg_id" BIGINT NOT NULL,
    "tg_name" VARCHAR(50) NOT NULL,
    "phone_number" VARCHAR(50) UNIQUE,
    "fullname" VARCHAR(100),
    "step" VARCHAR(50) NOT NULL,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "phonenumbers" (
    "tg_id" BIGINT NOT NULL,
    "fullname" VARCHAR(100),
    "phone_number" VARCHAR(50) UNIQUE,
    "status" BOOLEAN NOT NULL DEFAULT FALSE,
    "tulov" VARCHAR,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
)

;

CREATE TABLE IF NOT EXISTS "admins" (
    "username" VARCHAR,
    "passwordi" VARCHAR
);

INSERT INTO admins (username, passwordi) VALUES ('admin', 'test'); 